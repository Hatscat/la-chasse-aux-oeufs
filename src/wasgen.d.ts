declare module 'wasgen' {
  export interface Program {
    type: string
    gain?: Record<any, any>
    freq?: Record<any, any>
    Q?: number
    effect?: string
  }

  export default class SoundGenerator {
    play(
      program: Program | Program[],
      freq: number,
      velocity: number,
      startTime: number,
      releaseTime?: number
    ): void

    now(): number
  }
}
