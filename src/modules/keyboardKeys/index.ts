import { DeepReadonly } from 'ts-essentials'

const _keyIndex: { [code: string]: boolean } = {}

window.addEventListener(
  'keydown',
  (e) => {
    _keyIndex[e.code] = true
  },
  false
)

window.addEventListener(
  'keyup',
  (e) => {
    _keyIndex[e.code] = false
  },
  false
)

export const keyIndex = _keyIndex as DeepReadonly<typeof _keyIndex>
