import p5 from 'p5'
import { getChickenAnimationPosition } from './data/chicken'
import { getDirection } from './data/control'
import { Cell, CellType, createEmptyCell, createGrid, indexToPosition } from './data/grid'
import {
  createPlayer,
  getPlayerAnimationPosition,
  getPlayerColor,
  movePlayer,
  Player,
} from './data/player'
import { Point } from './data/point'
import { GameScreen } from './data/screen'
import {
  playBasketFillSound,
  playCannotPickUpChickenSound,
  playEggCollectSound,
  playerChickenPickUpSound,
  playGameOverSound,
  playGrassCutSound,
  playPlayersCollisionSound,
  playRockCollisionSound,
} from './data/sounds'
import { mutableStore, readonlyStore } from './storage'
;(() => {
  const playerCount = Number(/\d/.exec(location.hash)?.[0]) || 2
  setupGameData(playerCount)
  // eslint-disable-next-line no-new
  new p5(sketch)
})()

function setupGameData(playerCount: number) {
  const { rowCount, colCount } = readonlyStore

  for (let i = 0; i < playerCount; i += 1) {
    mutableStore.players.push(createPlayer(i, playerCount))
  }

  mutableStore.grid = createGrid()

  const chickenCellIndex = (rowCount >> 1) * colCount + colCount - 2
  mutableStore.chicken.currentCellIndex = mutableStore.chicken.oldCellIndex = chickenCellIndex
  mutableStore.grid[chickenCellIndex] = createEmptyCell(false)
}

function sketch(p: p5): p5 {
  p.setup = createSetup(p)
  p.draw = createDrawLoop(p)
  return p
}

function createSetup(p: p5): () => void {
  return () => {
    p.createCanvas(innerWidth, innerHeight)
    p.textAlign(p.CENTER, p.CENTER)
    p.textStyle(p.BOLD)
    p.textSize(readonlyStore.cellSize)
    mutableStore.backgroundImage = createBackgroundImage(p)
  }
}

function createDrawLoop(p: p5): () => void {
  return () => {
    mutableStore.time = Date.now()
    switch (readonlyStore.currentScreen) {
      case GameScreen.Play: {
        update()
        draw(p)
        break
      }
      case GameScreen.GameOver: {
        gameOverScreen(p)
        break
      }
      default:
        break
    }
  }
}

function update() {
  mutableStore.players.forEach(updatePlayer)
}

function updatePlayer(player: Player): void {
  const { maxEggCountPerPlayer, time, moveAnimationDuration, chicken, players } = readonlyStore
  const dir = getDirection(player.id)
  if (dir === null) return

  movePlayer(player, dir)
  const isFirstMove = player.animationEndTime === time + moveAnimationDuration

  const cell = mutableStore.grid[player.revealedCellIndex]
  if (player.revealedCellIndex === chicken.currentCellIndex) {
    if (isFirstMove) {
      if (player.eggCount === 0) {
        mutableStore.chicken.playerId = player.id
        playerChickenPickUpSound()
      } else {
        playCannotPickUpChickenSound()
      }
    }
  } else if (
    cell.type === CellType.Egg &&
    cell.hasEgg &&
    cell.playerId === player.id &&
    player.eggCount < maxEggCountPerPlayer
  ) {
    cell.hasEgg = false
    player.eggCount += 1
    playEggCollectSound(player.id)
  } else if (
    cell.type === CellType.Basket &&
    cell.playerId === player.id &&
    chicken.playerId === player.id
  ) {
    player.score += player.eggCount
    playGameOverSound()
    mutableStore.currentScreen = GameScreen.GameOver
  } else if (cell.type === CellType.Basket && cell.playerId === player.id && player.eggCount > 0) {
    player.score += player.eggCount
    player.eggCount = 0
    playBasketFillSound()
  } else if (cell.type === CellType.Rock && isFirstMove) {
    playRockCollisionSound()
  } else if (cell.isHidden) {
    playGrassCutSound()
  } else if (
    isFirstMove &&
    players.some(
      (otherPlayer) =>
        otherPlayer.id !== player.id && otherPlayer.currentCellIndex === player.revealedCellIndex
    )
  ) {
    playPlayersCollisionSound()
  }
  cell.isHidden = false
}

function draw(p: p5) {
  const { grid, players, backgroundImage } = readonlyStore
  if (backgroundImage) {
    p.image(backgroundImage, 0, 0)
  }
  grid.forEach((cell, cellIndex) => drawCell(p, cell, cellIndex))
  drawChiken(p)
  players.forEach((player) => drawPlayer(p, player))
}

function createBackgroundImage(p: p5): p5.Graphics {
  const { cellSize } = readonlyStore
  const tileSize = cellSize >> 1
  const noiseRate = 5
  const g = p.createGraphics(p.width, p.height)
  g.strokeWeight(0)
  g.background(0)

  for (let row = 0; row < p.height / tileSize; row += 1) {
    for (let col = 0; col < p.width / tileSize; col += 1) {
      const k = p.noise(
        ((col * tileSize) / p.width) * noiseRate,
        ((row * tileSize) / p.height) * noiseRate
      )
      g.fill(p.lerpColor(p.color('#6b8e23'), p.color('#2e8b57'), k))

      g.rect(col * tileSize, row * tileSize, tileSize, tileSize)
    }
  }
  return g
}

function drawCell(p: p5, cell: Cell, cellIndex: number): void {
  const { players } = readonlyStore
  p.strokeWeight(0)
  const position = indexToPosition(cellIndex)
  if (cell.isHidden) {
    p.text(cell.hiddenEmoji, position.x, position.y)
    return
  }
  if (cell.type === CellType.Rock) {
    p.text('⛰️', position.x, position.y)
    return
  }
  if (cell.type === CellType.Egg && cell.hasEgg) {
    drawEgg(p, cell.playerId, position)
    return
  }
  if (cell.type === CellType.Basket) {
    p.fill(getPlayerColor(cell.playerId))
    p.stroke('#000')
    p.strokeWeight(2)
    p.text('❑', position.x, position.y)
    p.textSize(readonlyStore.cellSize >> 1)
    p.text(players[cell.playerId].score, position.x, position.y)
    p.textSize(readonlyStore.cellSize)
    p.strokeWeight(0)
  }
}

function drawPlayer(p: p5, player: Player): void {
  const { time, cellSize, maxEggCountPerPlayer } = readonlyStore
  const { x, y } = getPlayerAnimationPosition(player)

  p.text(player.avatar, x, y)

  for (let i = 0; i < player.eggCount; i += 1) {
    const a = (i / maxEggCountPerPlayer) * Math.PI * 2 + time * 0.005
    const l = cellSize * (0.65 + Math.sin(time * 0.02) * 0.1)
    drawEgg(p, player.id, { x: x + Math.cos(a) * l, y: y - Math.sin(a) * l })
  }
}

function drawChiken(p: p5) {
  const { chicken } = readonlyStore
  const { x, y } = getChickenAnimationPosition()
  p.text(chicken.avatar, x, y)
}

function drawEgg(p: p5, playerId: number, position: Point): void {
  p.fill(getPlayerColor(playerId))
  p.stroke('#000')
  p.strokeWeight(1)
  p.textSize(readonlyStore.cellSize >> 1)
  p.text('✪', position.x, position.y)
  p.textSize(readonlyStore.cellSize)
}

function gameOverScreen(p: p5) {
  const { players, backgroundImage, cellSize, chicken, chickenScore, time } = readonlyStore
  const playerFinalScore = (player: Player) =>
    player.score + (chicken.playerId === player.id ? chickenScore : 0)
  const highScore = players.map(playerFinalScore).reduce((a, b) => Math.max(a, b))

  if (backgroundImage) {
    p.image(backgroundImage, 0, 0)
  }
  p.fill(63, 63)
  p.rect(0, 0, p.width, p.height)
  p.fill(255, 255)

  p.stroke('#000')
  p.strokeWeight(1)

  players.forEach((player) => {
    const x = (player.id + 1) / (players.length + 1)
    const jumpHeight =
      playerFinalScore(player) === highScore ? Math.abs(Math.sin(time * 0.006)) * cellSize : 0

    p.textSize(cellSize * 2)
    p.text(player.avatar, x * p.width, p.height / 2 - cellSize * 2 - jumpHeight)

    p.textSize(cellSize)
    p.fill(getPlayerColor(player.id))
    p.text(`${player.score} ✪`, x * p.width, p.height / 2)
    if (player.id === chicken.playerId) {
      p.text(`+ ${chickenScore} (${chicken.avatar})`, x * p.width, p.height / 2 + cellSize * 1.2)
    }
  })
  p.textSize(cellSize >> 1)
  p.fill(220)
  p.text('Recharger pour relancer une partie (F5)', p.width / 2, p.height - cellSize * 3)
}
