export interface Point {
  x: number
  y: number
}

export function squareDist(a: Point, b: Point): number {
  return (b.x - a.x) ** 2 + (b.y - a.y) ** 2
}

export function lerpPoint(start: Point, stop: Point, k: number): Point {
  return {
    x: lerp(start.x, stop.x, k),
    y: lerp(start.y, stop.y, k),
  }
}

export function lerp(start: number, stop: number, k: number): number {
  return start + (stop - start) * k
}
