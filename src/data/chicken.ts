import { readonlyStore } from '~/storage'
import { indexToPosition } from './grid'
import { lerp, Point } from './point'

export interface Chicken {
  avatar: string
  currentCellIndex: number
  oldCellIndex: number
  animationEndTime: number
  playerId: number | null
}

export function createChicken(cellIndex = 0): Chicken {
  return {
    avatar: '🐣',
    currentCellIndex: cellIndex,
    oldCellIndex: cellIndex,
    animationEndTime: 0,
    playerId: null,
  }
}

export function getChickenAnimationPosition(): Point {
  const { chicken, moveAnimationDuration, time, cellSize } = readonlyStore

  if (chicken.playerId === null) {
    return indexToPosition(chicken.currentCellIndex)
  }

  const k = Math.max(0, (chicken.animationEndTime - time) / moveAnimationDuration)
  const jumpHeight = Math.abs(Math.sin(k * Math.PI * 3)) * (cellSize * 0.2)

  const oldPosition = indexToPosition(chicken.oldCellIndex)
  const nextPosition = indexToPosition(chicken.currentCellIndex)
  return {
    x: lerp(nextPosition.x, oldPosition.x, k),
    y: lerp(nextPosition.y, oldPosition.y, k) - jumpHeight,
  }
}
