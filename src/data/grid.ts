import { readonlyStore } from '~/storage'
import { Point } from './point'

export type Cell = EmptyCell | RockCell | EggCell | BasketCell

export enum CellType {
  Empty,
  Egg,
  Rock,
  Basket,
}

interface CellBase {
  type: CellType
  isHidden: boolean
  hiddenEmoji: string
}

export interface EmptyCell extends CellBase {
  type: CellType.Empty
}

export interface RockCell extends CellBase {
  type: CellType.Rock
}

export interface EggCell extends CellBase {
  type: CellType.Egg
  hasEgg: boolean
  playerId: number
}

export interface BasketCell extends CellBase {
  isHidden: false
  type: CellType.Basket
  playerId: number
}

function getRandomHiddenEmoji(): string {
  const emojis = ['🌿', '🌿', '🌿', '🌿', '🌿', '🌿', '🌿', '🌿', '🌱', '🌱', '☘️', '🍀']
  return emojis[(Math.random() * emojis.length) | 0]
}

export function createEmptyCell(isHidden = true): EmptyCell {
  return {
    isHidden,
    hiddenEmoji: getRandomHiddenEmoji(),
    type: CellType.Empty,
  }
}

function createRockCell(): RockCell {
  return {
    isHidden: true,
    hiddenEmoji: getRandomHiddenEmoji(),
    type: CellType.Rock,
  }
}

function createEggCell(playerId: number): EggCell {
  return {
    isHidden: true,
    hiddenEmoji: getRandomHiddenEmoji(),
    type: CellType.Egg,
    hasEgg: true,
    playerId,
  }
}

function createBasketCell(playerId: number): BasketCell {
  return {
    isHidden: false,
    hiddenEmoji: '',
    type: CellType.Basket,
    playerId,
  }
}

export function createGrid(): Cell[] {
  const { rowCount, colCount, players, eggCellRate, rockCellRate } = readonlyStore
  const cellCount = rowCount * colCount - players.length * 2
  const eggCellCount = Math.round(cellCount * eggCellRate)
  const rockCellCount = Math.round(cellCount * rockCellRate)

  const grid: Cell[] = shuffle(
    Array(cellCount)
      .fill(0)
      .map((_, i) =>
        i < eggCellCount
          ? createEggCell(i % players.length)
          : i < rockCellCount + eggCellCount
          ? createRockCell()
          : createEmptyCell()
      )
  )

  players.forEach((player) => {
    grid.splice(player.currentCellIndex - 1, 0, createBasketCell(player.id), createEmptyCell(false))
  })

  return grid
}

export function indexToPosition(index: number): Point {
  const { colCount, cellSize } = readonlyStore
  return {
    x: (index % colCount) * cellSize + (cellSize >> 1),
    y: ((index / colCount) | 0) * cellSize + (cellSize >> 1),
  }
}

export function positionToIndex(position: Point): number {
  const { colCount, cellSize } = readonlyStore
  return ((position.y / cellSize) | 0) * colCount + ((position.x / cellSize) | 0)
}

function shuffle<T>(a: T[]): T[] {
  for (let i = a.length - 1; i > 0; i -= 1) {
    const j = Math.floor(Math.random() * (i + 1))
    ;[a[i], a[j]] = [a[j], a[i]]
  }
  return a
}
