import { mutableStore, readonlyStore } from '~/storage'
import { Direction } from './control'
import { CellType, indexToPosition } from './grid'
import { lerp, Point } from './point'

export interface Player {
  id: number
  avatar: string
  currentCellIndex: number
  oldCellIndex: number
  revealedCellIndex: number
  animationEndTime: number
  eggCount: number
  score: number
}

export function createPlayer(id: number, playerCount: number): Player {
  const { colCount, rowCount } = readonlyStore
  const rowSpacing = Math.floor(rowCount / (playerCount + 1))
  const delta = rowCount - (playerCount + 1) * rowSpacing
  const cellIndex = (id + 1) * rowSpacing * colCount + colCount * (delta >> 1) + 1
  return {
    id,
    avatar: ['🐰', '🦊', '🐻', '🐷', '🐸', '🐮', '🐭', '🦁', '🐹'][id],
    currentCellIndex: cellIndex,
    oldCellIndex: cellIndex,
    revealedCellIndex: cellIndex,
    animationEndTime: 0,
    eggCount: 0,
    score: 0,
  }
}

export function getPlayerColor(playerId: number): string {
  return (
    [
      '#e0ffff',
      '#ff8c00',
      '#8b4513',
      '#fa8072',
      '#adff2f',
      '#696969',
      '#ffff00',
      '#ff0000',
      '#9400d3',
    ][playerId] ?? ''
  )
}

export function movePlayer(player: Player, direction: Direction): void {
  const { colCount, moveAnimationDuration, time, chicken } = readonlyStore

  if (time < player.animationEndTime) return

  player.revealedCellIndex = handleMapLimits(
    player.currentCellIndex,
    player.currentCellIndex +
      {
        [Direction.Left]: -1,
        [Direction.Right]: 1,
        [Direction.Up]: -colCount,
        [Direction.Down]: colCount,
      }[direction]
  )

  const nextCellIndex = handleCollisions(player, player.currentCellIndex, player.revealedCellIndex)

  if (player.revealedCellIndex !== player.currentCellIndex) {
    player.animationEndTime = time + moveAnimationDuration
  }
  if (nextCellIndex !== player.currentCellIndex) {
    if (chicken.playerId === player.id) {
      mutableStore.chicken.animationEndTime = player.animationEndTime
      mutableStore.chicken.oldCellIndex = player.oldCellIndex
      mutableStore.chicken.currentCellIndex = player.currentCellIndex
    }
    player.oldCellIndex = player.currentCellIndex
    player.currentCellIndex = nextCellIndex
  } else if (player.revealedCellIndex === chicken.currentCellIndex) {
    player.oldCellIndex = player.revealedCellIndex
  }
}

function handleMapLimits(oldCellIndex: number, nextCellIndex: number): number {
  const { grid, colCount } = readonlyStore
  return nextCellIndex >= 0 &&
    nextCellIndex < grid.length &&
    ((nextCellIndex % colCount) - (oldCellIndex % colCount)) ** 2 <= 1
    ? nextCellIndex
    : oldCellIndex
}

function handleCollisions(player: Player, oldCellIndex: number, nextCellIndex: number): number {
  const { grid, players, chicken } = readonlyStore
  return grid[nextCellIndex].type !== CellType.Rock &&
    grid[nextCellIndex].type !== CellType.Basket &&
    (nextCellIndex !== chicken.currentCellIndex || chicken.playerId === player.id) &&
    players.every((otherPlayer) => otherPlayer.currentCellIndex !== nextCellIndex)
    ? nextCellIndex
    : oldCellIndex
}

export function getPlayerAnimationPosition(player: Player): Point {
  const { moveAnimationDuration, time, cellSize } = readonlyStore
  const k = Math.max(0, (player.animationEndTime - time) / moveAnimationDuration)
  const jumpHeight = Math.abs(Math.sin(k * Math.PI * 2)) * (cellSize * 0.4)

  if (player.currentCellIndex === player.revealedCellIndex) {
    const oldPosition = indexToPosition(player.oldCellIndex)
    const nextPosition = indexToPosition(player.currentCellIndex)
    return {
      x: lerp(nextPosition.x, oldPosition.x, k),
      y: lerp(nextPosition.y, oldPosition.y, k) - jumpHeight,
    }
  }

  const currentPosition = indexToPosition(player.currentCellIndex)
  const revealedPosition = indexToPosition(player.revealedCellIndex)
  const amt = 1 - Math.sin(k * Math.PI) / 2
  return {
    x: lerp(revealedPosition.x, currentPosition.x, amt),
    y: lerp(revealedPosition.y, currentPosition.y, amt) - jumpHeight,
  }
}
