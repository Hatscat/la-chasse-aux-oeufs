import { keyIndex } from '~/modules/keyboardKeys'

export enum Direction {
  Left,
  Right,
  Up,
  Down,
}

export function getDirection(playerId: number): Direction | null {
  const gamepad = navigator.getGamepads()[playerId]
  if (gamepad) {
    const [horizontalValue, verticalValue] = gamepad.axes
    const threshold = 0.5
    if (horizontalValue < -threshold) return Direction.Left
    if (horizontalValue > threshold) return Direction.Right
    if (verticalValue < -threshold) return Direction.Up
    if (verticalValue > threshold) return Direction.Down
  }

  const directionKeyCodes = playersDirectionKeyCodes[playerId]
  if (directionKeyCodes) {
    if (keyIndex[directionKeyCodes.left]) return Direction.Left
    if (keyIndex[directionKeyCodes.right]) return Direction.Right
    if (keyIndex[directionKeyCodes.up]) return Direction.Up
    if (keyIndex[directionKeyCodes.down]) return Direction.Down
  }

  return null
}

const playersDirectionKeyCodes = [
  {
    left: 'ArrowLeft',
    right: 'ArrowRight',
    up: 'ArrowUp',
    down: 'ArrowDown',
  },
  {
    left: 'KeyA',
    right: 'KeyD',
    up: 'KeyW',
    down: 'KeyS',
  },
  {
    left: 'KeyJ',
    right: 'KeyL',
    up: 'KeyI',
    down: 'KeyK',
  },
  {
    left: 'Digit4',
    right: 'Digit6',
    up: 'Digit8',
    down: 'Digit2',
  },
] as const
