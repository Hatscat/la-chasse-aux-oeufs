import { readonlyStore } from '~/storage'

/** Sounds generated with https://andyhall.github.io/wafxr/ */

export function playRockCollisionSound(): void {
  const now = readonlyStore.soundGenerator.now()
  readonlyStore.soundGenerator.play(
    [
      { type: 'nb', gain: { a: 0.02, h: 0, d: 0.012, s: 0.36, r: 0.048 } },
      { type: 'bandpass', freq: { t: 1.2, p: 0.7, q: 0.04 }, Q: 2.5 },
    ],
    477,
    1,
    now,
    now + 0.05
  )
}

export function playGrassCutSound(): void {
  const now = readonlyStore.soundGenerator.now()
  readonlyStore.soundGenerator.play(
    [
      { type: 'np', gain: { a: 0.038, h: 0, d: 0.066, s: 0.23, r: 0.015 } },
      { type: 'bandpass', freq: { t: 2, p: 0.4, q: 0.02 }, Q: 1.9 },
    ],
    514,
    1,
    now,
    now + 0.02
  )
}

export function playEggCollectSound(playerId: number): void {
  const startTime = readonlyStore.soundGenerator.now()
  const releaseTime = startTime + 0.15

  const play = [
    () =>
      readonlyStore.soundGenerator.play(
        [
          {
            type: 'triangle',
            freq: { w: 0.02, t: 1.2, f: 0, a: 0, p: 1, q: 0.25, x: 1 },
            gain: { a: 0.1, h: 0, d: 0.1, s: 0.21, r: 0.104 },
          },
        ],
        1414,
        1,
        startTime,
        releaseTime
      ),
    () =>
      readonlyStore.soundGenerator.play(
        [
          {
            type: 'w909',
            freq: { w: 0.04, t: 1.45, f: 0, a: 0, p: 1, q: 0.25, x: 1 },
            gain: { a: 0.1, h: 0, d: 0.1, s: 0.13, r: 0.136 },
          },
        ],
        131,
        1,
        startTime,
        releaseTime
      ),
    () =>
      readonlyStore.soundGenerator.play(
        [{ type: 'w999', gain: { a: 0.1, h: 0, d: 0.1, s: 0.33, r: 0.24 } }],
        545,
        1,
        startTime,
        releaseTime
      ),
    () =>
      readonlyStore.soundGenerator.play(
        [
          {
            type: 'w999',
            freq: [
              { w: 0.01, t: 1, f: 0, a: 0, p: 2.3, q: 0.11, x: 1 },
              { w: 0.05, t: 1.3, f: 0, a: 0, p: 1, q: 0.25, x: 1 },
            ],
            gain: { a: 0.1, h: 0, d: 0.1, s: 0.59, r: 0.091 },
          },
        ],
        255,
        1,
        startTime,
        releaseTime
      ),
    () =>
      readonlyStore.soundGenerator.play(
        [{ type: 'w999', gain: { a: 0.1, h: 0, d: 0.1, s: 0.23, r: 0.162 } }],
        287,
        1,
        startTime,
        releaseTime
      ),
    () =>
      readonlyStore.soundGenerator.play(
        [{ type: 'sine', gain: { a: 0.1, h: 0, d: 0.1, s: 0.1, r: 0.143 } }],
        285,
        1,
        startTime,
        releaseTime
      ),
    () =>
      readonlyStore.soundGenerator.play(
        [
          {
            type: 'w999',
            freq: { w: 0.01, t: 1, f: 0, a: 0, p: 1.25, q: 0.28, x: 1 },
            gain: { a: 0.1, h: 0, d: 0.1, s: 0.52, r: 0.106 },
          },
        ],
        279,
        1,
        startTime,
        releaseTime
      ),
    () =>
      readonlyStore.soundGenerator.play(
        [
          {
            type: 'w909',
            freq: [
              { w: 0.01, t: 1, f: 0, a: 0, p: 1.5, q: 0.11, x: 1 },
              { w: 0.08, t: 0.75, f: 0, a: 0, p: 1, q: 0.25, x: 1 },
            ],
            gain: { a: 0.1, h: 0, d: 0.1, s: 0.52, r: 0.118 },
          },
        ],
        777,
        1,
        startTime,
        releaseTime
      ),
    () =>
      readonlyStore.soundGenerator.play(
        [
          {
            type: 'triangle',
            freq: { w: 0.05, t: 1.3, f: 0, a: 0, p: 1, q: 0.25, x: 1 },
            gain: { a: 0.1, h: 0, d: 0.1, s: 0.46, r: 0.149 },
          },
        ],
        121,
        1,
        startTime,
        releaseTime
      ),
  ][playerId]

  if (play) {
    play()
  }
}

export function playBasketFillSound(): void {
  const now = readonlyStore.soundGenerator.now()
  readonlyStore.soundGenerator.play(
    [
      {
        type: 'triangle',
        freq: [
          { w: 0.1, t: 1, f: 0, a: 0, p: 1.35, q: 0.12, x: 1 },
          { type: 'square', freq: 47, gain: { t: 0.08 } },
        ],
        gain: { a: 0.1, h: 0, d: 0.1, s: 0.21, r: 0.143 },
      },
    ],
    918,
    1,
    now,
    now + 0.15
  )
}

export function playerChickenPickUpSound(): void {
  const now = readonlyStore.soundGenerator.now()
  readonlyStore.soundGenerator.play(
    [
      {
        type: 'triangle',
        freq: [
          { w: 0.1, t: 1, f: 0, a: 0, p: 1.2, q: 0.08, x: 1 },
          { type: 'square', freq: 28, gain: { t: 0.17 } },
        ],
        gain: { a: 0.1, h: 0, d: 0.1, s: 0.36, r: 0.051 },
      },
    ],
    2349,
    1,
    now,
    now + 0.161
  )
}

export function playCannotPickUpChickenSound(): void {
  const now = readonlyStore.soundGenerator.now()
  readonlyStore.soundGenerator.play(
    [
      {
        type: 'sawtooth',
        freq: { w: 0.1, t: 1, f: 0, a: 0, p: 0.7, q: 0.05, x: 1 },
        gain: { a: 0.1, h: 0, d: 0.1, s: 0.29, r: 0.028 },
      },
    ],
    319,
    1,
    now,
    now + 0.24
  )
}

export function playGameOverSound(): void {
  const now = readonlyStore.soundGenerator.now()
  readonlyStore.soundGenerator.play(
    [
      {
        type: 'sine',
        freq: [
          { w: 0.09, t: 1.1, f: 200, a: 0, p: 0.85, q: 0.15, x: 20 },
          { type: 'square', freq: 16.7, gain: { t: 0.19 } },
        ],
        gain: { a: 0.1, h: 0, d: 0.1, s: 0.49, r: 0.186 },
      },
    ],
    1760,
    1,
    now,
    now + 0.188
  )
}

export function playPlayersCollisionSound(): void {
  const now = readonlyStore.soundGenerator.now()
  readonlyStore.soundGenerator.play(
    [
      {
        type: 'p25',
        freq: { w: 0.1, t: 1, f: 0, a: 0, p: 0.5, q: 0.04, x: 1 },
        gain: [
          { a: 0.1, h: 0, d: 0.1, s: 0.5, r: 0.08 },
          { type: 'sine', freq: 30, gain: { t: 0.38, r: 0.08, z: 0 } },
        ],
      },
    ],
    231,
    1,
    now,
    now + 0.11
  )
}
