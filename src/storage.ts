import p5 from 'p5'
import { DeepReadonly } from 'ts-essentials'
import SoundGenerator from 'wasgen'
import { createChicken } from './data/chicken'
import { Cell } from './data/grid'
import { Player } from './data/player'
import { GameScreen } from './data/screen'

export const mutableStore = {
  time: 0,
  cellSize: 52 as const,
  eggCellRate: 0.2 as const,
  rockCellRate: 0.08 as const,
  moveAnimationDuration: 500 as const,
  maxEggCountPerPlayer: 3 as const,
  chickenScore: 3 as const,
  currentScreen: GameScreen.Play,
  get rowCount(): number {
    return (innerHeight / readonlyStore.cellSize) | 0
  },
  get colCount(): number {
    return (innerWidth / readonlyStore.cellSize) | 0
  },
  players: [] as Player[],
  grid: [] as Cell[],
  chicken: createChicken(),
  backgroundImage: null as p5.Graphics | null,
  soundGenerator: new SoundGenerator(),
}

export const readonlyStore = mutableStore as DeepReadonly<typeof mutableStore>
